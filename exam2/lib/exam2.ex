defmodule Exam2 do
  require Float
  require Integer

  def tag(t) do
    bmi = elem(Float.parse(t["bmi"]), 0)
    cond do
      bmi < 18.5 -> "Bajo peso"
      bmi >= 18.5 and bmi < 25 -> "Normal"
      bmi >= 25 and bmi < 30 -> "Sobre peso"
      bmi >= 30 -> "Obeso"
    end
  end

  def count_by_bmi_cat() do
    File.stream!("insurance.csv")
    |> CSV.decode!(headers: true)
    |> Enum.map(&tag/1)
    |> Enum.frequencies
  end

  def relation_smoker_children() do
    File.stream!("insurance.csv")
    |> CSV.decode!(headers: true)
    |> Enum.group_by(fn x -> x["children"] end)
    |> Enum.map(fn {children, rows} -> {children, Enum.count(rows), Enum.filter(rows, fn x -> x["smoker"] == "yes" end)} end)
    |> Enum.map(fn {children, total, smokers} -> {children,  %{"smoker" => Enum.count(smokers)/total, "no smoker" => (total - Enum.count(smokers))/ total}} end)
    |> Enum.into(%{}, fn {children, data} -> {elem(Integer.parse(children), 0), data} end)
  end
end
